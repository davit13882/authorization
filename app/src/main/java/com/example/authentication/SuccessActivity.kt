package com.example.authentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_success.*

class SuccessActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success)

        init()
    }

    private fun init() {
        val intent = intent
        val successText = intent.getStringExtra("SuccessMessage")

        successMessage.text = successText


    }
}